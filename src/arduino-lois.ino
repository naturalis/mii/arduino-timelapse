// ============================================================================
// "Leven na de dood" interactive
//
// Draaischijf interface
//
// Exhibit: Door aan een schijf te draaien wordt een timelapse film afgespeeld
//
// Techniek: Draaischijf is gekoppeld aan een rotary encoder (Bourns ena1j-b28-l00128)
//           Deze encoder is aangesloten op een arduino (Leonardo) microcontroller. 
//           Deze Arduino is via een usb verbinding aangesloten op een PC. 
//           Deze PC ontvangt de data van de arduino en speelt de timelapse film af 
//
// Werking:  De encoder heeft twee signaalaansluitingen, A en B. Deze uitgangen geven per omwenteling
//           beide 128 blokgolf pulsen af. 
//           Het signaal A is verbonden met een interrupt ingang van de arduino. De interrupt routine 
//           verzamelt informatie over snelheid en richting van de beweging.
//           Indien er aan de schijf gedraaid is wordt de informatie over richting en snelheid via een 
//           serieel signaal naar de PC gestuurd.
//
// Aansluitingen:
//           De Encoder heeft vier aansluitingen. Ze ijn als volgt op de Arduino aangesloten.
//           Encoder "-" is verbonden met Arduino GND  
//           Encoder "A" is verbonden met Arduino 0 (Digital)  
//           Encoder "+" is verbonden met Arduino 5V  
//           Encoder "B" is verbonden met Arduino 1 (Digital)  
//      
//           De Arduino is via een USB verbinding aangesloten op de PC en krijgt hierdoor zijn voeding.
//
// Serieel signaal:
//           Via de USB verbinding wordt een serieel signaal verzonden. 
//           Baudrate: 115200, 8 bits, No parity, 1 stopbit 
// Format serieel signaal:
//           Per record 6 ascii characters.
//           Char 1: richting indicator. Hoofdletter U (up) richting met de klok mee. D (down) tegen klok in.
//           Char 2: spatie
//           char 3: snelheid tientallen. Snelheid is een getal tussen 1 en 32
//           char 4: snelheid eentallen
//           char 5: CR ascii $0D
//           char 6: LF ascii $0A
//
//  Snelheidsberekening:
//           De interval tussen de interrups die door de neergaande flank van signaal A worden getriggerd
//           wordt gemeten in microseconden. Omdat bij lage snelheid het aantal microsec. enorm fluctueert terwijl
//           dit bij hoge snelheid juist minder is wordt gebruik gemaakt van een logarithmisch filter. Hierdoor
//           ontstaat een min of meer linear verband tussen de draaisnelheid en het snelheidsgetal. 
//           Om de snelheidsindicatie nog verder te versoepelen wordt dit signaal nog eens geintegreerd waardoor 
//           een voortschijdend gemiddelde ontstaat.


  

#include <math.h>

#define Enc_A 1
#define Enc_B A5

#define MaxIntegr 5

const float fT0 = 224000; // Constanten voor berekening snelheid
const float fN0 = 9.0;

volatile boolean Moved = false; // true als er beweging is gemeten
volatile boolean Dir = true; // true is met klok mee, false is andersom
volatile unsigned long SysMicros; // Variabelen voor meten van snelheid
volatile unsigned long MyMicros;

int Integr[MaxIntegr];  // integer array voor het middelen van de outputwaardes.

void setup() 
{
  Serial.begin(115200);
  delay(1000);
  pinMode(Enc_A,INPUT);
  pinMode(Enc_B,INPUT);
  attachInterrupt(digitalPinToInterrupt(Enc_A),Enc_A_Intr,FALLING); // Interrupt als signaal van hoog naar laag gaat
}

void Enc_A_Intr()
{
unsigned long Micros;

  Moved = true;
  Micros = micros();
  MyMicros = Micros - SysMicros; // Meet verstreken tijd na vorige intr. in microseconden
  SysMicros = Micros;
  if(digitalRead(Enc_B) == HIGH)
  {
    Dir = true;
  }
  else
  {
    Dir = false;
  }
}


void DataOutRaw(void)  // test functie
{
    if(Dir)
      Serial.print("U");  
    else
      Serial.print("D");  
      
      Serial.print(" ");
      Serial.println(MyMicros);
}


int iNr(float fInterval) // Snelheids berekening
  {
    if (fInterval > 200500) // ondergrens = interval > 200500 microsec.
    {
      return 1;
    }
    if (fInterval < 5500) // bovengrens = inteval < 5500 microsec.
    {
      return 32;
    }
  float f = -fN0 * log(fInterval / fT0);
  return f;
}  

int Average(int V) // Deze functie genereert een voortschijdend gemiddelde.
{                  // de mate waarin dit filter werkt is in te stellen met de waarde van MaxIntergr 
int i;
int Tot = 0;
 for (i=1;i<MaxIntegr ;i++)
  Integr[i-1] = Integr[i];
 Integr[MaxIntegr-1]=V;
 for (i=0;i<MaxIntegr ;i++)
   Tot += Integr[i]; 
  return (Tot / MaxIntegr);
}

void DataOut(void)
{
int Speed = 0;
int MyAvr;
  
    if(Dir)
      Serial.print("U");  // richting indicator (up = rechtsom)
    else
      Serial.print("D");  // richting indicator (down = linksom)
    MyAvr = Average(iNr(MyMicros));
    if (MyAvr < 10)
      Serial.print(" 0"); 
    else  
      Serial.print(" ");
    Serial.println(MyAvr);
}


void loop() 
{
  if( Moved ) // Als er beweging is gemeten volgt output
  {
    Moved = false;
    DataOut();
  }
}
