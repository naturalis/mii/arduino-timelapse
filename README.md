# arduino-timelapse

## "Leven na de dood" interactive

### Draaischijf interface

#### Exhibit:

Door aan een schijf te draaien wordt een timelapse film afgespeeld

#### Techniek:

* Draaischijf is gekoppeld aan een rotary encoder (Bourns ena1j-b28-l00128)
* De encoder is aangesloten op een arduino (Leonardo) microcontroller.
* De Arduino is via een usb verbinding aangesloten op een PC.
* De PC ontvangt de data van de arduino en speelt de timelapse film af.

#### Werking:

De encoder heeft twee signaalaansluitingen, A en B. Deze uitgangen geven per omwenteling beide 128 blokgolf pulsen af.
Het signaal A is verbonden met een interrupt ingang van de arduino.
De interrupt routine verzamelt informatie over snelheid en richting van de beweging.
Indien er aan de schijf gedraaid is wordt de informatie over richting en snelheid via een serieel signaal naar de PC gestuurd.

#### Aansluitingen:

De Encoder heeft vier aansluitingen. Ze ijn als volgt op de Arduino aangesloten.

* Encoder "-" is verbonden met Arduino GND
* Encoder "A" is verbonden met Arduino 0 (Digital)
* Encoder "+" is verbonden met Arduino 5V
* Encoder "B" is verbonden met Arduino 1 (Digital)

De Arduino is via een USB verbinding aangesloten op de PC en krijgt hierdoor zijn voeding.

#### Serieel signaal:

* Via de USB verbinding wordt een serieel signaal verzonden.
* Baudrate: 115200, 8 bits, No parity, 1 stopbit

#### Format serieel signaal:

Per record 6 ascii characters.

* Char 1: richting indicator. Hoofdletter U (up) richting met de klok mee. D (down) tegen klok in.
* Char 2: spatie
* char 3: snelheid tientallen. Snelheid is een getal tussen 1 en 32
* Char 4: snelheid eentallen
* Char 5: CR ascii $0D
* Char 6: LF ascii $0A

#### Snelheidsberekening:

De interval tussen de interrups die door de neergaande flank van signaal A worden getriggerd
wordt gemeten in microseconden. Omdat bij lage snelheid het aantal microsec. enorm fluctueert terwijl
dit bij hoge snelheid juist minder is wordt gebruik gemaakt van een logarithmisch filter. Hierdoor
ontstaat een min of meer linear verband tussen de draaisnelheid en het snelheidsgetal.
Om de snelheidsindicatie nog verder te versoepelen wordt dit signaal nog eens geintegreerd waardoor
een voortschijdend gemiddelde ontstaat.
