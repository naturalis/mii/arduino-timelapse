#include <Keyboard.h>

int buttonPin1 = 12;  // Set a button to any pin
int buttonPin2 = 5;  // Set a button to any pin
int buttonPin3 = 7;  // Set a button to any pin
int buttonPin4 = 9;  // Set a button to any pin

void setup()
{
  pinMode(buttonPin1, INPUT);  // Set the button as an input
  digitalWrite(buttonPin1, HIGH);  // Pull the button high
  pinMode(buttonPin2, INPUT);  // Set the button as an input
  digitalWrite(buttonPin2, HIGH);  // Pull the button high
  pinMode(buttonPin3, INPUT);  // Set the button as an input
  digitalWrite(buttonPin3, HIGH);  // Pull the button high 
  pinMode(buttonPin4, INPUT);  // Set the button as an input
  digitalWrite(buttonPin4, HIGH);  // Pull the button high
}

void loop()
{
  if (digitalRead(buttonPin1) == 0)  // if the button goes low
  {
    Keyboard.write('v');  // send a 'a' to the computer via Keyboard HID
    delay(400);  // delay so there aren't a kajillion a's
  }
  if (digitalRead(buttonPin2) == 0)  // if the button goes low
  {
    Keyboard.write('b');  // send a 'b' to the computer via Keyboard HID
    delay(500);  // delay so there aren't a kajillion b's
  }
  if (digitalRead(buttonPin3) == 0)  // if the button goes low
  {
    Keyboard.write('n');  // send a 'c' to the computer via Keyboard HID
    delay(500);  // delay so there aren't a kajillion c's
  }
  if (digitalRead(buttonPin4) == 0)  // if the button goes low
  {
    Keyboard.write('m');  // send a 'd' to the computer via Keyboard HID
    delay(500);  // delay so there aren't a kajillion d's
  }
}
